from django.urls import path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from .views import VendedorList, VendedorCreate, VendedorUpdate , VendedorDelete
from rest_framework.urlpatterns import format_suffix_patterns
from apps.Vendedor import views

urlpatterns = [
    path('listar/', VendedorList.as_view(), name="vendedores_list"),
    path('crear/', VendedorCreate.as_view(), name="vendedores_form"),
    path('editar/<int:pk>', VendedorUpdate.as_view(), name="vendedores_update"),
    path('borrar/<int:pk>', VendedorDelete.as_view(), name="vendedores_borrar"),
    path('api/', views.API_objects.as_view()),
    path('api/<int:pk>/', views.API_objects_details.as_view()),
    
]

urlpatterns = format_suffix_patterns(urlpatterns)

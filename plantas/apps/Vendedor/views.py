from django.shortcuts import render, redirect
from django.contrib.auth.mixins import UserPassesTestMixin, AccessMixin, LoginRequiredMixin
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Vendedor
from .forms import VendedorForm
from rest_framework import generics
from .serializers import VendedorSerializer


class VendedorList (ListView):                    
    model = Vendedor
    template_name = 'Vendedor/vendedor_list.html'

class VendedorCreate (CreateView):
    model = Vendedor
    form_class = VendedorForm
    template_name = 'Vendedor/vendedor_form.html'
    success_url = reverse_lazy('vendedores_list')

class VendedorUpdate(UpdateView):
    model = Vendedor
    form_class = VendedorForm
    template_name = 'Vendedor/vendedor_form.html'
    success_url = reverse_lazy('vendedores_list')

class VendedorDelete(DeleteView):
    model = Vendedor
    template_name = 'Vendedor/Vendedor_borrar.html'
    success_url = reverse_lazy('vendedores_list')




class API_objects(generics.ListCreateAPIView):
    queryset = Vendedor.objects.all()
    serializer_class = VendedorSerializer
    
class API_objects_details(generics.RetrieveUpdateDestroyAPIView):
    queryset = Vendedor.objects.all()
    serializer_class = VendedorSerializer
